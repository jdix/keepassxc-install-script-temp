#!/bin/bash
# set -x
# functions
# as_sudoers(){
#   su $SUDO_USER -c "$@"
# }

cleanup(){
  echo "clean up keepassxc git/build dir"
  rm -rf ./keepassxc
}

# begin
if [[ $# -gt 1 ]]; then
  echo "only one option supported at a time, $# were supplied"
  exit 1
fi

while test $# -gt 0  
do   
  case $1 in
    keepdeb)  echo "keepdeb option was set" && keepdeb=true >&2 ;;     
    uninstall)  echo "uninstall option was set" && uninstall=true >&2 ;;
    *)  echo "Invalid option: $1" && exit 1 >&2 ;;   
  esac 
  shift
done 

if [[ $(id -u) -ne 0 ]]; then
  if [[ -z ${SUDO_USER} ]]; then
    echo "must run with sudo"
    exit 1
  fi
fi
# if [[ $(id -u) -ne 0 && ${SUDO_USER} -ne '' ]] ; then echo "must run as root with sudo" ; exit 1 ; fi

echo "This scipt installs the latest keepassxc from source control:
  - using working directory as a workdir
  - idempotently
  - dpkg systems only (ubuntu)
"

# keepassx build docs: https://github.com/keepassxreboot/keepassxc/wiki/Building-KeePassXC
echo "cloning keepassxc to check latest ver"
git clone https://github.com/keepassxreboot/keepassxc.git --single-branch --branch master &>/dev/null
cd keepassxc
# check for current installed version, exit if already present
# todo: don't think this handles tags with "-beta"
LATEST_KEEPASSXC_VER=$(git tag -l | tail -n 1)
DPKG_KEEPASSXC_LINE=$(dpkg -l | grep keepassxc | grep -o -P '\d.\d.\d' | head -n 1)

# if there's an installed version, but not to current version, remove it so it can be installed!
if [[ ( ${DPKG_KEEPASSXC_LINE} != ${LATEST_KEEPASSXC_VER} ) || $uninstall == 'true' ]]; then
  echo "removing old/existing version of keepassxc"
  dpkg -r keepassxc &>/dev/null
  if [[ $uninstall == 'true' ]]; then
    exit 0
  fi
fi

# if current ver installed, just exit, nothing to do!
if [[ ${DPKG_KEEPASSXC_LINE} == ${LATEST_KEEPASSXC_VER} ]]; then
  echo "latest keepass already installed"
  cd .. && cleanup
  exit 0
fi

mkdir build
cd build
echo "running make commands"
cmake -DWITH_XC_ALL=ON -DCMAKE_BUILD_TYPE=Release .. &>/dev/null
make -j8 &>/dev/null
# remove excess doc files and empty dirs
echo "cleaning make files from doc dir"
find ./docs/ -executable -o ! -regex '.*[kK]ee[pP]ass.*' -exec rm -if {} \+
find ./docs/ -type d -print | sort -r | xargs rmdir &>/dev/null
# don't use make install, using dpkg/checkinstall allows easy uninstall :)
# make install
echo "creating deb package and installing"
checkinstall --install=yes --fstrans=yes --pkgname=keepassxc --pkgversion=${LATEST_KEEPASSXC_VER} --default &>/dev/null
if [[ $keepdeb == 'true' ]]; then
  echo "copying deb to /tmp/ dir"
  cp keepassxc*.deb /tmp/ # for debug to keep extra package around
fi

cd ../.. && cleanup
